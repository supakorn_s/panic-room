using UnityEngine;

public class InteractableObject_Subtitle : InteractableObject
{
    [SerializeField][TextArea(5, 15)] private string _textToDisplay = "Write Something Here.";
    [SerializeField] private float _textTimer = 1.0f;
    public override void ObjectInteract(PlayerBrain playerBrain)
    {
        SubtitleDisplay.Instance.TypeSubtitle(_textToDisplay, _textTimer);
    }
}
