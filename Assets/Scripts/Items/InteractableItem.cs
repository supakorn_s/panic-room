using UnityEngine;

public class InteractableItem : MonoBehaviour
{
    [Header("Item")]
    [SerializeField] private ItemProfileClass _itemProfile;
    public ItemProfileClass itemProfile => _itemProfile;
}
