using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item_##_Scene_ItemName", menuName = "Profile/Pickable Item Profile", order = 0)]
public class PickableItemProfile : ItemProfileClass
{
    
}
