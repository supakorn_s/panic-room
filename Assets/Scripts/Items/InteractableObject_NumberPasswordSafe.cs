using UnityEngine;

public class InteractableObject_NumberPasswordSafe : InteractableObject
{
    [Header("Safe Property")]
    [SerializeField] private int _password = 1234;
    [SerializeField] private bool _isLocked = false;
    [SerializeField] private bool _isOpened = false;
    [SerializeField] private float _doorDelay = 1.0f;
    public int password => _password;
    private float _doorDelayTimer;

    [Header("Safe Animation")]
    [SerializeField] private Animator _doorAnim;
    [SerializeField] private AnimationClip _doorOpenIdle;
    [SerializeField] private AnimationClip _doorCloseIdle;
    [SerializeField] private AnimationClip _doorOpen;
    [SerializeField] private AnimationClip _doorClose;

    [Header("Safe Audio")]
    [SerializeField] private AudioSource _doorAudioSource;
    [SerializeField] private AudioClip _openSound;
    [SerializeField] private AudioClip _closeSound;
    [SerializeField] private AudioClip _lockedSound;
    [SerializeField] private AudioClip _unlockedSound;

    [Header("Door Subtitle")]
    [SerializeField] private float _lockedSubtitleTimer = 3.0f;
    [SerializeField] private string _onLocked = "It need a password.";
    [SerializeField] private float _unlockedSubtitleTimer = 3.0f;
    [SerializeField] private string _onUnlock = "It's now unlock.";

    private void Start()
    {
        StartDoorCheck();
    }
    private void Update()
    {
        if (_doorDelayTimer > 0)
        {
            _doorDelayTimer -= Time.deltaTime;
        }
    }
    public override void ObjectInteract(PlayerBrain playerBrain)
    {
        if (_isLocked)
        {
            PlayerPasswordUI.Instance.OpenPasswordNumberpanel(GetComponent<InteractableObject_NumberPasswordSafe>());
            PlayOneShotWithRandomPitch(_lockedSound);
            //SubtitleDisplay.Instance.TypeSubtitle(_onLocked, _lockedSubtitleTimer);
        }
        else
        {
            OpenDoor();
        }
    }
    public void CheckingPassWordInput(int input)
    {
        if (input == _password)
        {
            PlayOneShotWithRandomPitch(_unlockedSound);
            _isLocked = false;
            SubtitleDisplay.Instance.TypeSubtitle(_onUnlock, _unlockedSubtitleTimer);
            PlayerPasswordUI.Instance.ButtonClosePasswordNumberpanel();
        }
        else
        {
            PlayOneShotWithRandomPitch(_lockedSound);
        }
    }
    private void OpenDoor()
    {
        if (_doorDelayTimer <= 0)
        {
            switch (_isOpened)
            {
                case true:
                    _isOpened = false;
                    _doorAnim.Play(_doorClose.name, 0, 0);
                    _doorDelayTimer = _doorDelay;
                    PlayOneShotWithRandomPitch(_closeSound);
                    break;
                case false:
                    _isOpened = true;
                    _doorAnim.Play(_doorOpen.name, 0, 0);
                    _doorDelayTimer = _doorDelay;
                    PlayOneShotWithRandomPitch(_openSound);
                    break;
            }
        }
    }
    private void StartDoorCheck()
    {
        switch (_isOpened)
        {
            case true:
                _doorAnim.Play(_doorOpenIdle.name, 0, 0);
                break;
            case false:
                _doorAnim.Play(_doorCloseIdle.name, 0, 0);
                break;
        }
    }
    public void ChangeDoorState(bool isOpen)
    {
        _isOpened = isOpen;

        switch (_isOpened)
        {
            case true:
                _doorAnim.Play(_doorClose.name, 0, 0);
                _doorDelayTimer = _doorDelay;
                PlayOneShotWithRandomPitch(_closeSound);
                break;
            case false:
                _doorAnim.Play(_doorOpen.name, 0, 0);
                _doorDelayTimer = _doorDelay;
                PlayOneShotWithRandomPitch(_openSound);
                break;
        }

    }
    public void ChangeDoorLockState(bool isLocked)
    {
        _isLocked = isLocked;
        PlayOneShotWithRandomPitch(_lockedSound);
    }
    private void PlayOneShotWithRandomPitch(AudioClip ClipToPlay)
    {
        _doorAudioSource.pitch = Random.Range(0.8f, 1.2f);
        _doorAudioSource.PlayOneShot(ClipToPlay);
    }
}
