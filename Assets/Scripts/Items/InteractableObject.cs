using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractableObject : MonoBehaviour
{
    public virtual void ObjectInteract(PlayerBrain playerBrain)
    {
        Debug.Log("Do something");
    }
}
