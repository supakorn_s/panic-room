using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LightController : MonoBehaviour
{
    [SerializeField] private Light[] _lights;
    [SerializeField] private float _transitionTime = 0.5f;
    [HideInInspector] public List<float> intensity;

    private void Awake()
    {
        for (int i = 0; i < _lights.Length; i++)
        {
            intensity.Add(_lights[i].intensity);
        }
    }
    public void TurnOnLight(bool turnLight)
    {
        for (int i = 0; i < _lights.Length; i++)
        {
            _lights[i].enabled = true;

            if (turnLight)
            {
                _lights[i].DOIntensity(intensity[i], _transitionTime);
            }
            else
            {
                _lights[i].DOIntensity(0, _transitionTime);
            }
        }
    }

    public void ShutAllLightImmediately()
    {
        for (int i = 0; i < _lights.Length; i++)
        {
            _lights[i].enabled = false;
            _lights[i].DOIntensity(0, _transitionTime);
        }
    }
}
