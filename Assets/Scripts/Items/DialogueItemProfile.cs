using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item_##_Scene_ItemName", menuName = "Profile/Dialogue Item Profile", order = 2)]
public class DialogueItemProfile : ItemProfileClass
{
    [Header("Special Function")]
    [SerializeField] private ComputerDialogueGraph _dialogueGraph;
    [SerializeField] private bool _destroyedOnUsed = false;
    public override string UseItem()
    {
        string Action = "ChangeDialogueProfile";
        return Action;
    }

    public override ComputerDialogueGraph GetDialogue()
    {
        return _dialogueGraph;
    }
    public override bool GetBool()
    {
        return _destroyedOnUsed;
    }
}
