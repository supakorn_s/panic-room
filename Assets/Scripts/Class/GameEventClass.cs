using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Event_EventName", menuName = "Profile/Event", order = 1)]
public class GameEventClass : ScriptableObject
{
    [InspectorButton("Raise")]
    public bool raiseE;

    private readonly List<IGameEventListener> _eventListener = new List<IGameEventListener>();

    public void Raise()
    {
        for (int i = _eventListener.Count-1; i >= 0; i--)
        {
            //Debug.Log(this.name);
            _eventListener[i].OnEventRaised(this.name);
        }
    }
    public void RegisterListener(IGameEventListener listener)
    {
        if (!_eventListener.Contains(listener))
        {
            _eventListener.Add(listener);
        }
    }
    public void UnRegisterListener(IGameEventListener listener)
    {
        if (_eventListener.Contains(listener))
        {
            _eventListener.Remove(listener);
        }
    }
}
