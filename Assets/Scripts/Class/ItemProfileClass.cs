using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "Item_ItemName", menuName = "Profile/Item Profile", order = 0)]
public abstract class ItemProfileClass : ScriptableObject
{
    [SerializeField] protected int _itemID;
    [SerializeField] protected string _itemName;
    [SerializeField] [TextArea] protected string _itemDescription;
    [SerializeField] protected Sprite _itemIcon;
    [SerializeField] protected GameObject _itemPrefab;

    [Header("Item Inspection Function")]
    [SerializeField] protected bool _usable;
    [SerializeField] protected bool _droppable;


    #region Public Getter
    public int itemID => _itemID;
    public string itemName => _itemName;
    public string itemDescription => _itemDescription;
    public Sprite itemIcon => _itemIcon;
    public GameObject itemPrefab => _itemPrefab;
    public bool usable => _usable;
    public bool droppable => _droppable;
    #endregion

    public virtual string PickItem()
    {
        string Action = "PickUpItem";
        Debug.Log("PickUpItem");
        return Action;
    }
    public virtual string UseItem()
    {
        string Action = "UseItem";
        Debug.Log("UseItem");
        return Action;
    }
    public virtual string InspectItem()
    {
        string Action = "InspectItem";
        Debug.Log("InspectItem");
        return Action;
    }
    public virtual string DropItem()
    {
        string Action = "DropItem";
        Debug.Log("DropItem");
        return Action;
    }
    public virtual ComputerDialogueGraph GetDialogue()
    {
        return null;
    }
    public virtual bool GetBool()
    {
        bool returnThis = false;
        return returnThis;
    }
}
