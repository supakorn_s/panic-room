﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

[CreateAssetMenu(fileName = "Dialogue_##_Scene_Name", menuName = "Graph", order = 0)]
public class ComputerDialogueGraph : NodeGraph {

	public BaseNode current;

}