﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public class DialogueNode : BaseNode
{

    [Input] public int entry;
    [Output] public int exit;

    [Header("Dialogue Text")]
    [TextArea(5, 15)][SerializeField] private string _dialogue;
    [Space(10)][SerializeField] private float _waitFor = -5;
    [Space(10)] [SerializeField] private GameEventClass _event;

    public override string GetString()
    {
        return "DialogueNode/" + _dialogue;
    }
    public override float GetWaitForTimer()
    {
        return _waitFor;
    }
    public override GameEventClass GetEvent()
    {
        return _event;
    }
}