﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public class BaseNode : Node {

	public virtual string GetString()
    {
        return null;
    }
    public virtual int GetInt()
    {
        return 0;
    }
    public virtual int GetTotalKeywordSet(int WordNumber)
    {
        return 0;
    }
    public virtual WordBankBranch GetWordEnum(int KeyNumber)
    {
        return WordBankBranch.NONE;
    }
    public virtual int GetTotalKeyAnswer()
    {
        return 0;
    }
    public virtual string GetKeyAnswer(int KeyNumber)
    {
        return null;
    }
    public virtual float GetWaitForTimer()
    {
        return 0.0f;
    }
    public virtual string GetBranchOutputPortName()
    {
        return null;
    }
    public virtual KeyWordSet GetKeyWordSet(int KeyWordSet)
    {
        return null;
    }
    public virtual GameEventClass GetEvent()
    {
        return null;
    }
}

[System.Serializable]
public class KeyWordSet
{
    [SerializeField] private string _keyAnswer;
    [SerializeField] private WordBankBranch _WordSet;
    public string keyAnswer => _keyAnswer;
    public WordBankBranch WordSet => _WordSet;
}