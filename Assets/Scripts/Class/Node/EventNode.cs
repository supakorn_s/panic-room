using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventNode : BaseNode
{
    [Input] public int entry;
    [Output] public int exit;
    [Space(10)] [SerializeField] private GameEventClass _event;

    public override string GetString()
    {
        return "EventNode";
    }
    public override GameEventClass GetEvent()
    {
        return _event;
    }
}
