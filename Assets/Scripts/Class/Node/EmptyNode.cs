using UnityEngine;

public class EmptyNode : BaseNode
{
    [Input] public int entry;
    [Output] public int exit;

    public override string GetString()
    {
        return "EmptyNode";
    }
}
