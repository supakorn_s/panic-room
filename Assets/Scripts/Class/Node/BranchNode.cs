﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public class BranchNode : BaseNode {

    [Input] public int entry;
    //[Output(dynamicPortList = true)] public int[] exit;

    [Output(dynamicPortList = true)] [SerializeField] private KeyWordSet[] _keyWordSet;
    [Space(10)] [SerializeField] private GameEventClass _event;

    public override string GetString()
    {
        return "BranchNode";
    }
    public override WordBankBranch GetWordEnum(int KeyNumber)
    {
        return _keyWordSet[KeyNumber].WordSet;
    }
    public override string GetKeyAnswer(int KeyNumber)
    {
        return _keyWordSet[KeyNumber].keyAnswer;
    }
    public override int GetTotalKeyAnswer()
    {
        return _keyWordSet.Length;
    }
    public override string GetBranchOutputPortName()
    {
        return "_keyWordSet";
    }
    public override KeyWordSet GetKeyWordSet(int KeyWordSet)
    {
        return _keyWordSet[KeyWordSet];
    }
    public override GameEventClass GetEvent()
    {
        return _event;
    }

}