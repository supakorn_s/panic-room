using UnityEngine;

public class InputController : MonoBehaviour
{
    private static InputController _instance;
    public static InputController Instance => _instance;

    public InputMaster _inputMaster;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        _inputMaster = new InputMaster();
    }

    private void OnEnable()
    {
        _inputMaster.Enable();
    }
    private void OnDisable()
    {
        _inputMaster.Disable();
    }

    public Vector2 WASD => _inputMaster.Player.WASD.ReadValue<Vector2>();
    public float Shift => _inputMaster.Player.Shift.ReadValue<float>();
    public float RightHold => _inputMaster.Player.RightHold.ReadValue<float>();
    public float Ctrl => _inputMaster.Player.Ctrl.ReadValue<float>();
    public bool ESC => _inputMaster.Player.Esc.triggered;
    public bool Spacebar => _inputMaster.Player.Spacebar.triggered;
    public bool Console => _inputMaster.Player.Console.triggered;
    public bool Tab => _inputMaster.Player.Tab.triggered;
    public bool LeftClick => _inputMaster.Player.LeftClick.triggered;

    public bool Interact => _inputMaster.Player.Interact.triggered;

    public bool Enter => _inputMaster.Player.Enter.triggered;
    public bool DebugMenu => _inputMaster.Player.F10.triggered;
    public bool DebugRestart => _inputMaster.Player.F9.triggered;

    public void EnableMouseLook() => _inputMaster.Player.MouseLook.Enable();

    public void DisableMouseLook() => _inputMaster.Player.MouseLook.Disable();
}
