using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;

public class PlayerController : MonoBehaviour
{
    [Header("Player State")]
    private PlayerBrain _playerBrain;
    public playerMovementState MovementState = playerMovementState.MOVEABLE;
    public playerStanceState StanceState = playerStanceState.STAND;

    #region Basic Settings
    [Header("Controller Feature")]
    public bool enableRun = true;
    public bool enableCrouch = true;
    public bool enableJump = true;
    public bool enableZoom = true;

    [Header("Movement value")]
    [SerializeField] private float _normalSpeed = 3.0f;
    [SerializeField] private float _crouchSpeed = 1.5f;
    [SerializeField] private float _runSpeed = 4.0f;
    private CharacterController _cc;

    [Header("Jump value")]
    [SerializeField] private float _jumpHeight = 1.0f;
    [SerializeField] private float _gravityValue = -9.81f;
    [SerializeField] private float _gravityCrouchValue = -9.81f / 1.5f;

    [Header("Zoom value")]
    [SerializeField] private float _normalFOV = 60.0f;
    [SerializeField] private float _runningFOV = 75.0f;
    [SerializeField] private float _zoomFOV = 20.0f;
    [SerializeField] private float _zoomTransitionSpeed = 3.0f;

    [Header("Crouch value")]
    [SerializeField] private GameObject _cam;
    [SerializeField] private float _camTransitionSpeed = 5.0f;
    [SerializeField] private float _normalHeight = 1.8f;
    [SerializeField] private float _crouchHeight = 1.0f;
    [SerializeField] private Vector3 _normalCenter = new Vector3(0, 0, 0);
    [SerializeField] private Vector3 _crouchCenter = new Vector3(0, -0.4f, 0);
    [SerializeField] private Vector3 _camNormalHeight = new Vector3(0, 0.8f, 0);
    [SerializeField] private Vector3 _camCrouchHeight = new Vector3(0, 0.0f, 0);
    private bool _isCrouch;

    [Header("Camera and Mouse Control")]
    [SerializeField] private Transform[] _playerBody;
    [SerializeField] private CinemachineVirtualCamera _playerCam;
    [SerializeField] private CinemachineInputProvider _camInputProvider;
    [SerializeField] private InputActionReference _mouseMovement;
    [SerializeField] private string _mainCameraTag = "MainCamera";
    [SerializeField] private float _mouseSensitivityGain = 0.25f;
    [SerializeField] private LayerMask _playerMask;
    private Transform _cameraTransform;
    private CinemachinePOV _playerPOV;
    #endregion

    #region Player Input
    private bool _isGrounded;
    private bool _isRunning;
    private bool _isWalking;
    private Vector3 _playerVelocity;
    private Vector2 _inputMovement;
    private Vector3 _move;
    private float _currentSpeed;
    private Coroutine _zooming;

    public bool isGrounded => _isGrounded;
    public bool isRunning => _isRunning;
    public bool isWalking => _isWalking;
    #endregion

    private void Awake()
    {
        _playerBrain = GetComponent<PlayerBrain>();
        _cc = GetComponent<CharacterController>();
        _cameraTransform = GameObject.FindGameObjectWithTag(_mainCameraTag).transform;
        _playerPOV = _playerCam.GetCinemachineComponent<CinemachinePOV>();

        //_playerCam.gameObject.transform.SetParent(null);
        _playerCam.gameObject.transform.eulerAngles = Vector3.zero;
        _playerCam.m_Lens.FieldOfView = _normalFOV;
        _playerPOV.m_VerticalAxis.m_MaxSpeed = _mouseSensitivityGain;
        _playerPOV.m_HorizontalAxis.m_MaxSpeed = _mouseSensitivityGain;

        Cursor.lockState = CursorLockMode.Locked;

        _playerCam.transform.eulerAngles = _playerCam.Follow.transform.localEulerAngles;
    }
    private void Update()
    {
        if (MovementState == playerMovementState.MOVEABLE)
        {
            CinemachineInputCheck();
            switch (StanceState)
            {
                case playerStanceState.STAND:
                    CharacterMovement();
                    Jumping();
                    Zooming();
                    break;
                case playerStanceState.COUCH:
                    CharacterMovement();
                    Zooming();
                    break;
            }
        }
        else
        {
            NotMovingState();
        }
    }
    private void CharacterMovement()
    {
        _isGrounded = _cc.isGrounded;
        if (_isGrounded && _playerVelocity.y < 0)
        {
            _playerVelocity.y = 0f;
        }

        _inputMovement = InputController.Instance.WASD;
        _move = new Vector3(_inputMovement.x, 0f, _inputMovement.y);
        _move = _cameraTransform.forward * _move.z + _cameraTransform.right * _move.x;
        _move.y = 0f;
        if (_inputMovement.x == 0 && _inputMovement.y == 0 && _isGrounded) _isWalking = false; else _isWalking = true;

        Running();
        Crouching();

        _move = _move.normalized * _currentSpeed * Time.deltaTime;
        _cc.Move(_move);

        BodyRotation();
        //CinemachineInputCheck();
    }
    private void BodyRotation()
    {
        float targetAngle = _cameraTransform.eulerAngles.y;
        //transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);
        for (int i = 0; i < _playerBody.Length; i++)
        {
            _playerBody[i].rotation = Quaternion.Euler(0f, targetAngle, 0f);
        }
    }
    private void Running()
    {
        if (InputController.Instance.Shift == 1 && _inputMovement.y > 0 && enableRun) _isRunning = true;
        else _isRunning = false;

        if (_isRunning) _currentSpeed = _runSpeed;
        if (_isCrouch) _currentSpeed = _crouchSpeed;

        if (!_isRunning && !_isCrouch) _currentSpeed = _normalSpeed;
    }
    private void Crouching()
    {
        if (InputController.Instance.Ctrl == 1 && _isGrounded && StanceState == playerStanceState.STAND && enableCrouch)
        {
            _isCrouch = true;
        }
        if (InputController.Instance.Ctrl == 0 && StanceState == playerStanceState.COUCH)
        {
            if (CheckCrouchingCollision == false)
            {
                _isCrouch = false;
            }
        }

        switch (_isCrouch)
        {
            case true:
                StanceState = playerStanceState.COUCH;
                _cc.height = _crouchHeight;
                _cc.center = _crouchCenter;
                _isGrounded = true;
                if (_cam.transform.localPosition.y != _camCrouchHeight.y)
                {
                    float smooth = Mathf.Lerp(_cam.transform.localPosition.y, _camCrouchHeight.y, _camTransitionSpeed * Time.deltaTime);
                    _cam.transform.localPosition = new Vector3(_cam.transform.localPosition.x, smooth, _cam.transform.localPosition.z);
                    //Vector3 currentPos = _cam.transform.localPosition;
                    //Vector3 targetPos = new Vector3(_cam.transform.localPosition.x, _camCrouchHeight.y, _cam.transform.localPosition.z);
                    //_cam.transform.localPosition = Vector3.Lerp(currentPos, targetPos, 0.5f);
                    if ((_cam.transform.localPosition.y - _camCrouchHeight.y) <= 0.01f)
                    {
                        _cam.transform.localPosition = new Vector3(_cam.transform.localPosition.x, _camCrouchHeight.y, _cam.transform.localPosition.z);
                    }
                }
                else
                {
                    _playerVelocity.y += _gravityCrouchValue * Time.deltaTime;
                    _cc.Move(_playerVelocity * Time.deltaTime);
                }
                break;
            case false:
                StanceState = playerStanceState.STAND;
                _cc.height = _normalHeight;
                _cc.center = _normalCenter;
                if (_cam.transform.localPosition.y != _camNormalHeight.y)
                {
                    float smooth2 = Mathf.Lerp(_cam.transform.localPosition.y, _camNormalHeight.y, _camTransitionSpeed * Time.deltaTime);
                    _cam.transform.localPosition = new Vector3(_cam.transform.localPosition.x, smooth2, _cam.transform.localPosition.z);
                    if ((_camNormalHeight.y - _cam.transform.localPosition.y) <= 0.01f)
                    {
                        _cam.transform.localPosition = new Vector3(_cam.transform.localPosition.x, _camNormalHeight.y, _cam.transform.localPosition.z);
                    }
                }
                break;
        }
    }
    private void Jumping()
    {
        if (InputController.Instance.Spacebar && _isGrounded && enableJump)
        {
            _playerVelocity.y += Mathf.Sqrt(_jumpHeight * -3.0f * _gravityValue);
        }
        _playerVelocity.y += _gravityValue * Time.deltaTime;
        _cc.Move(_playerVelocity * Time.deltaTime);
        CeilingCollisionCheck();
    }
    private void Zooming()
    {
        if (enableZoom)
        {
            if (InputController.Instance.RightHold == 1)
            {
                if (_playerCam.m_Lens.FieldOfView != _zoomFOV && !isRunning)
                {
                    _playerCam.m_Lens.FieldOfView = Mathf.Lerp(_playerCam.m_Lens.FieldOfView, _zoomFOV, _zoomTransitionSpeed * Time.deltaTime);
                    if ((_playerCam.m_Lens.FieldOfView - _zoomFOV) <= 0.1) _playerCam.m_Lens.FieldOfView = _zoomFOV;
                }
                if (_isRunning)
                {
                    if (_playerCam.m_Lens.FieldOfView != _runningFOV)
                    {
                        _playerCam.m_Lens.FieldOfView = Mathf.Lerp(_playerCam.m_Lens.FieldOfView, _runningFOV, _zoomTransitionSpeed * Time.deltaTime);
                        if ((_runningFOV - _playerCam.m_Lens.FieldOfView) <= 0.1) _playerCam.m_Lens.FieldOfView = _runningFOV;
                    }
                }
            }
            if (InputController.Instance.RightHold == 0)
            {
                switch (_isRunning)
                {
                    case false:
                        if (_playerCam.m_Lens.FieldOfView != _normalFOV)
                        {
                            _playerCam.m_Lens.FieldOfView = Mathf.Lerp(_playerCam.m_Lens.FieldOfView, _normalFOV, _zoomTransitionSpeed * Time.deltaTime);
                            if (_playerCam.m_Lens.FieldOfView > _normalFOV)
                            {
                                if ((_playerCam.m_Lens.FieldOfView - _normalFOV) <= 0.1) _playerCam.m_Lens.FieldOfView = _normalFOV;
                            }
                            else
                            {
                                if ((_normalFOV - _playerCam.m_Lens.FieldOfView) <= 0.1) _playerCam.m_Lens.FieldOfView = _normalFOV;
                            }
                        }
                        break;
                    case true:
                        if (_playerCam.m_Lens.FieldOfView != _runningFOV)
                        {
                            _playerCam.m_Lens.FieldOfView = Mathf.Lerp(_playerCam.m_Lens.FieldOfView, _runningFOV, _zoomTransitionSpeed * Time.deltaTime);
                            if ((_runningFOV - _playerCam.m_Lens.FieldOfView) <= 0.1) _playerCam.m_Lens.FieldOfView = _runningFOV;
                        }
                        break;
                }
            }
        }
    }
    private void NotMovingState()
    {
        Jumping();
        CinemachineInputCheck();
        _isWalking = false;
    }
    private void CeilingCollisionCheck()
    {
        if ((_cc.collisionFlags & CollisionFlags.Above) != 0)
        {
            _playerVelocity.y = -_playerVelocity.y;
        }
    }
    private bool CheckCrouchingCollision
    {
        get
        {
            bool collisionAbove = false;
            Vector3 startPos = transform.position + new Vector3(0, _crouchHeight - (_normalHeight * 0.5f), 0);
            float length = (_normalHeight - _crouchHeight);
            //Debug.DrawRay(startPos, Vector3.up, Color.red);
            if (Physics.Raycast(startPos, Vector3.up, length))
            {
                collisionAbove = true;
            }
            return collisionAbove;
        }
    }
    private void CinemachineInputCheck() // prevent player camera from moving when not wanted
    {
        switch (MovementState)
        {
            case playerMovementState.MOVEABLE:
                if (PlayerInterfaceManager.Instance.isPaused)
                {
                    if (_camInputProvider.XYAxis == _mouseMovement) _camInputProvider.XYAxis = null;
                }
                else
                {
                if (_camInputProvider.XYAxis == null) _camInputProvider.XYAxis = _mouseMovement;
                }
                break;
            case playerMovementState.UNMOVEABLE:
                if (_camInputProvider.XYAxis == _mouseMovement) _camInputProvider.XYAxis = null;
                break;
        }
    }
}

public enum playerMovementState { MOVEABLE, UNMOVEABLE };
public enum playerStanceState { STAND, COUCH };