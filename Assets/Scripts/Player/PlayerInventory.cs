using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    [Header("Inventory Informations")]
    [SerializeField] private GameObject _itemDropPosition;

    [SerializeField] private int _totalInventorySlot = 3;
    public int totalInventorySlot => _totalInventorySlot;

    [Header("Current Inventory")]
    [SerializeField] private List<ItemProfileClass> _currentItem; public List<ItemProfileClass> currentItem => _currentItem;

    public bool AddNewItemToInventory(ItemProfileClass itemProfile)
    {
        bool successPickup = false;
        int freeSlot = PlayerInventoryUI.Instance.CheckTheFirstFreeSlot;
        if (freeSlot >= 0)
        {
            ItemSlot slot = PlayerInventoryUI.Instance.itemSlot[freeSlot];
            slot.AddItemProfile(itemProfile);
            ItemPickUpNotification.Instance.SpawnItemNotification(itemProfile);
            _currentItem[freeSlot] = itemProfile;
            successPickup = true;
        }
        return successPickup;
    }
    public void InventorySlotUpdate()
    {
        for (int i = 0; i < PlayerInventoryUI.Instance.itemSlot.Count; i++)
        {
            if (PlayerInventoryUI.Instance.itemSlot[i]._itemProfile == null)
            {
                _currentItem[i] = null;
            }
            else
            {
                _currentItem[i] = PlayerInventoryUI.Instance.itemSlot[i]._itemProfile;
            }

        }
    }
    public void SpawnDropItem(ItemProfileClass _itemProfile)
    {
        GameObject droppedItem = (GameObject)Instantiate(_itemProfile.itemPrefab, _itemDropPosition.transform.position, Quaternion.identity);
    }
    public bool SearchItemInInventory(ItemProfileClass _itemToSearch)
    {
        bool SearchResult = false;

        for (int i = 0; i < _currentItem.Count; i++)
        {
            if (_itemToSearch == _currentItem[i])
            {
                SearchResult = true;
                return SearchResult;
            }
        }

        return SearchResult;
    }
    public void DeleteItem(ItemProfileClass _itemProfile)
    {
        PlayerInventoryUI.Instance.DeleteItem(_itemProfile);
    }
}
