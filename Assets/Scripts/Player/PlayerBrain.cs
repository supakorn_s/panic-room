using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBrain : SingletonClass<PlayerBrain>
{
    [Header("Player Components")]
    [SerializeField] private PlayerController _playerController;
    [SerializeField] private PlayerRaycast _playerRaycast;
    [SerializeField] private PlayerInventory _playerInventory;

    #region Getter
    public PlayerController playerController => _playerController;
    public PlayerRaycast playerRaycast => _playerRaycast;
    public PlayerInventory playerInventory => _playerInventory;
    #endregion

    private void OnEnable()
    {
        _playerController.enabled = true;
        _playerRaycast.enabled = true;
        _playerInventory.enabled = true;
    }
    private void OnDisable()
    {
        _playerController.enabled = false;
        _playerRaycast.enabled = false;
        _playerInventory.enabled = false;
    }
}
