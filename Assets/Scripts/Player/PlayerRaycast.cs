using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRaycast : MonoBehaviour
{
    private PlayerBrain _playerBrain;
    [Header("Raycast Setting")]
    [SerializeField] private LayerMask _aimMask;
    [SerializeField] private string[] _interactLayer;
    [SerializeField] private GameObject _rayCastStart;
    [SerializeField] private string _mainCameraTag = "MainCamera";
    [SerializeField] private float _rayCastDistance = 3;

    [Header("Private Reference")]
    private GameObject _hitObject;
    private Transform _camera;
    private PlayerController _playerController;
    private PlayerInventory _PlayerInventory;

    #region Getter
    public GameObject hitObject => _hitObject;
    #endregion

    private void Awake()
    {
        _playerBrain = gameObject.GetComponent<PlayerBrain>();
        _playerController = _playerBrain.playerController;
        _PlayerInventory = _playerBrain.playerInventory;
        _camera = GameObject.FindGameObjectWithTag(_mainCameraTag).transform;
    }
    private void Update()
    {
        RaycastStartRotation();

        switch (_playerController.MovementState)
        {
            case playerMovementState.MOVEABLE:
                PickupRaycastShoot();
                break;
        }
    }

    private void PickupRaycastShoot()
    {
        RaycastHit hit;
        if (Physics.Raycast(_rayCastStart.transform.position, _rayCastStart.transform.forward, out hit, _rayCastDistance, _aimMask))
        {
            _hitObject = null;
            for (int i = 0; i < _interactLayer.Length; i++)
            {
                if (hit.collider.gameObject.layer == LayerMask.NameToLayer(_interactLayer[i].ToString()))
                {
                    _hitObject = hit.collider.gameObject;
                    break;
                }
                if (i == _interactLayer.Length)
                {
                    _hitObject = null;
                    return;
                }
            }

            if (_hitObject == null) return;

            if (_hitObject.TryGetComponent(out InteractableItem interactableItem))
            {
                if (InputController.Instance.Interact)
                {
                    // Item Pickup
                    if (_PlayerInventory.AddNewItemToInventory(interactableItem.itemProfile))
                    {
                        Destroy(_hitObject);
                        _hitObject = null;
                        return;
                    }
                }
            }
            if (_hitObject.TryGetComponent(out InteractableObject interactableObject ))
            {
                if (InputController.Instance.Interact)
                {
                    interactableObject.ObjectInteract(_playerBrain);
                    return;
                }
            }
            /*else if (_hitObject.TryGetComponent(out InteractableObject interactableObject))
            {
                if (!_holdingsomething)
                {
                    if (InputController.Instance.Interact)
                    {
                        if (_hitObject.GetComponent<InteractableObject>().interactable)
                        {
                            _hitObject.GetComponent<InteractableObject>().trigger(false);
                        }
                    }
                }
            }*/
            if (_hitObject.TryGetComponent(out MZ_Controller mzController))
            {
                if (InputController.Instance.Interact)
                {
                    mzController.ChangeCameraPriority(true);
                }
            }
            Debug.DrawRay(_rayCastStart.transform.position, _rayCastStart.transform.TransformDirection(Vector3.forward) * hit.distance, Color.green);
        }
        else
        {
            _hitObject = null;

            Debug.DrawRay(_rayCastStart.transform.position, _rayCastStart.transform.TransformDirection(Vector3.forward) * _rayCastDistance, Color.red);
        }
    }
    private void RaycastStartRotation()
    {
        Vector3 targetAngle = _camera.eulerAngles;
        _rayCastStart.transform.rotation = Quaternion.Euler(targetAngle.x, targetAngle.y, targetAngle.z);
    }
}
