using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using RedBlueGames.Tools.TextTyper;
using XNode;

public class ComputerDialogueReader : MonoBehaviour
{
    [Header("Currently played dialogue")]
    public ComputerDialogueGraph currentGraph;

    [Header("Child Replica")]
    [SerializeField] private MZ_Replica[] _replica;
    public MZ_Replica[] replica => _replica;

    [Header("Audio")]
    [SerializeField] private int _soundDelay = 1;
    private int _soundDelayCounter = 0;
    [SerializeField] private AudioSource _typingAudioSource;
    [SerializeField] private AudioClip[] _typingSound;

    [Header("References")]
    [SerializeField] private TextTyper _computerMainTyper;
    [SerializeField] private MZ_WordListener _wordListener;
    public TextTyper computerMainTyper => _computerMainTyper;

    private BaseNode _currentNode;
    private Coroutine _parser;
    private string[] _dialogueData;
    private float _dialogueCountdown = -10.0f;

    private void Start()
    {
        // REPLICA START-UP

            for (int i = 0; i < _replica.Length; i++)
            {
                _replica[i].computerMainListener = _wordListener;
            }

        if (currentGraph == null) return;
        StartNewDialogue(currentGraph);
    }
    private void Update()
    {
        DialogueStatue();
    }
    public void StartNewDialogue(ComputerDialogueGraph newGraph) // Start a new dialogue
    {
        currentGraph = newGraph;

        foreach (BaseNode b in currentGraph.nodes)
        {
            switch (b.GetString())
            {
                case "Start":
                    currentGraph.current = b;
                    break;
            }
        }

        _parser = StartCoroutine(ParserNode());
    }
    public void PlayTypingSound(bool Delay)
    {
        if (Delay)
        {
            _soundDelayCounter--;

            if (_soundDelayCounter <= 0)
            {
                int randomSound = Random.Range(0, _typingSound.Length);
                _typingAudioSource.pitch = (Random.Range(1, 1.5f));
                _typingAudioSource.PlayOneShot(_typingSound[randomSound]);
                _soundDelayCounter = _soundDelay;
            }
        }
        else
        {
            int randomSound = Random.Range(0, _typingSound.Length);
            _typingAudioSource.pitch = (Random.Range(1, 1.5f));
            _typingAudioSource.PlayOneShot(_typingSound[randomSound]);
        }
    }

    IEnumerator ParserNode()    // Check node type
    {
        BaseNode b = currentGraph.current;
        _currentNode = b;
        string data = b.GetString();
        string[] dataParts = data.Split('/');
        _dialogueData = dataParts;

        if (dataParts[0] == "Start")
        {
            NextNode("exit");
        }
        if (dataParts[0] == "DialogueNode")
        {
            _computerMainTyper.TypeText(dataParts[1]);
            CheckForEvent(b);

            // REPLICA
            for (int i = 0; i < _replica.Length; i++)
            {
                _replica[i].computerReplicaTyper.TypeText(dataParts[1]);
            }
        }
        if (dataParts[0] == "BranchNode")
        {
            CheckForEvent(b);
            //_computerMainTyper.TypeText(dataParts[1]);
        }
        if (dataParts[0] == "EventNode")
        {
            CheckForEvent(b);
            NextNode("exit");
        }
        if (dataParts[0] == "EmptyNode")
        {
            NextNode("exit");
        }
        if (dataParts[0] == "End")
        {
            EndStopNode();
        }
        yield return null;
    }
    private void DialogueStatue()
    {
        // Run a count down timer before moving to the next dialogue in case there is a dialogue node connect to it.
        // -10 = the node will stay the same until call upon to move
        // -5 = move to next note, use for connect to BRANCH NODE
        if (currentGraph != null )
        {
            if (_computerMainTyper.IsTyping == false && _currentNode != null)
            {
                if (_currentNode.GetWaitForTimer() == -5)
                {
                    string nextNode = "exit";
                    _dialogueData = null;
                    _currentNode = null;
                    NextNode(nextNode);
                }

                if (_currentNode.GetWaitForTimer() > 0 && _dialogueCountdown < 0)
                {
                    _dialogueCountdown = _currentNode.GetWaitForTimer();
                }

                if (_dialogueCountdown > 0)
                {
                    _dialogueCountdown -= Time.deltaTime;
                    if (_dialogueCountdown <= 0)
                    {
                        _dialogueCountdown = -10;

                        string nextNode = "exit";
                        _dialogueData = null;
                        _currentNode = null;
                        NextNode(nextNode);
                    }
                }
            }
        }
        
    }
    #region Special Node Condition
    //field name list [ exit, _keyWordSet]
    public void NextNode(string fieldName)
    {
        if (_parser != null)
        {
            StopCoroutine(_parser);
            _parser = null;
        }

        foreach (NodePort p in currentGraph.current.Ports)
        {
            // Check if this port is the one we're looking for.
            if (p.fieldName == fieldName)
            {
                currentGraph.current = p.Connection.node as BaseNode;
                break;
            }
        }
        _parser = StartCoroutine(ParserNode());
    }
    public void CustomNextNode(string fieldName, int Output)
    {
        string newExit = fieldName + " " + Output;


        NextNode(newExit);
    }
    public void EndStopNode()
    {
        if (_parser != null)
        {
            StopCoroutine(_parser);
            _parser = null;
        }
        currentGraph = null;
    }
    private void CheckForEvent(BaseNode b)
    {
        if (b.GetEvent() != null) b.GetEvent().Raise();
    }
    #endregion

    #region Debug
    public void TestDialogueCompleteCheck()
    {
        if (_computerMainTyper.IsTyping == false)
        {
            if (_wordListener.playerWord != null)
            {
                // For Testing Word Bank purpose only
                if (_wordListener.PlayerTypedText == "continue")
                {
                    Debug.Log("Text match");
                    _wordListener.playerWord = null;

                    string nextNode = "exit";
                    _dialogueData = null;
                    _currentNode = null;
                    NextNode(nextNode);
                }
                else
                {
                    Debug.Log("Text not match");
                    _wordListener.playerWord = null;
                }
            }
            //Debug.Log("Typing Completed");
        }
    }
    #endregion
}
