using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RedBlueGames.Tools.TextTyper;
using TMPro;

public class MZ_Replica : MonoBehaviour // Control Child of the main MZ object
{
    [Header("References")]
    [SerializeField] private TextTyper _computerReplicaTyper;
    [SerializeField] TMP_InputField _inputfield;

    private MZ_Controller _mzController;
    [HideInInspector] public MZ_WordListener computerMainListener;
    public TextTyper computerReplicaTyper => _computerReplicaTyper;



    private void Start()
    {
        _mzController = GetComponent<MZ_Controller>();
    }
    private void Update()
    {
        SubmitTextToMain();
    }
    private void SubmitTextToMain()
    {
        if (_mzController == null || computerMainListener == null) return; 
        if (_mzController.isBeingUse && !_computerReplicaTyper.IsTyping)
        {
            if (InputController.Instance.Enter)
            {
                string inputText = _inputfield.text.ToLower();
                computerMainListener.WordBankCheck(inputText);

                _inputfield.text = null;
            }
        }
    }
    public void SetNotUsingThisMZ()
    {
        _mzController.ExitPC();
    }
}
