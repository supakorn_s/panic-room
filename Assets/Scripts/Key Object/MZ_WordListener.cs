using UnityEngine;
using TMPro;

public class MZ_WordListener : MonoBehaviour
{
    private MZ_Controller _mzController;
    private ComputerDialogueReader _comDialogueReader;

    [Header("Input Provider")]
    [SerializeField] TextMeshProUGUI _inputTextPlaceholder;
    [SerializeField] TextMeshProUGUI _inputTextbox;
    [SerializeField] TMP_InputField _inputfield;

    public TMP_InputField inputfield => _inputfield;

    [Header("Word Bank")]
    [SerializeField] private WordBank _wordBankLists;

    [HideInInspector] public string playerWord;

    [Header("Audio")]
    //[SerializeField] private int _soundDelay = 1;
    private int _soundDelayCounter = 0;
    [SerializeField] private AudioSource _typingAudioSource;
    [SerializeField] private AudioClip[] _typingSound;

    private void Start()
    {
        _mzController = GetComponent<MZ_Controller>();
        _comDialogueReader = GetComponent<ComputerDialogueReader>();
        _inputfield.interactable = false;
    }
    private void Update()
    {
        SubmitText();
    }
    private void SubmitText() // When submitting inputfield
    {
        if (_mzController.isBeingUse && !_comDialogueReader.computerMainTyper.IsTyping)
        {
            if (InputController.Instance.Enter)
            {
                string inputText = _inputfield.text.ToLower();
                playerWord = inputText;
                WordBankCheck(inputText);
                //Debug.Log(inputText);

                _inputfield.text = null;
            }
        }

        if (_comDialogueReader.computerMainTyper.IsTyping)
        {
            playerWord = null;
        }
    }
    public void WordBankCheck(string typedText)
    {
        if (typedText == "exit")
        {
            _mzController.ExitPC();
            for (int i = 0; i < _comDialogueReader.replica.Length; i++)
            {
                _comDialogueReader.replica[i].SetNotUsingThisMZ();
            }
            return;
        }
        //_comDialogueReader.DialogueCompleteCheck();

        string[] dataParts = typedText.Split(' ');
        BaseNode b = _comDialogueReader.currentGraph.current;
        int totalKeyAnswer = b.GetTotalKeyAnswer(); // Total Keyword that can be use to answer this node.
        WordBankBranch keyType = WordBankBranch.NONE; // Type of the enum for checking with Branch Node
        string playerKeyword = typedText;

        //INSPECT CHECK
        for (int i = 0; i < _wordBankLists.inspect.Length; i++)
        {
            if (typedText.Contains(_wordBankLists.inspect[i]))
            {
                keyType = WordBankBranch.INSPECT;

                playerKeyword = typedText.Replace(_wordBankLists.inspect[i] + " ", "");
            }
        }
        //INTERACT CHECK
        for (int i = 0; i < _wordBankLists.interact.Length; i++)
        {
            if (typedText.Contains(_wordBankLists.interact[i]))
            {
                keyType = WordBankBranch.INTERACT;

                playerKeyword = typedText.Replace(_wordBankLists.interact[i] + " ", "");
            }
        }
        //MOVE CHECK
        for (int i = 0; i < _wordBankLists.move.Length; i++)
        {
            if (typedText.Contains(_wordBankLists.move[i]))
            {
                keyType = WordBankBranch.MOVE;

                playerKeyword = typedText.Replace(_wordBankLists.move[i] + " ", "");
            }
        }
        CheckWordSetEnum(b, keyType, playerKeyword);
        //Debug.Log(keyType);
    } // Compare answer to wordbank
    private void CheckWordSetEnum(BaseNode b, WordBankBranch keyType, string playerKeyword) // Check from enum word set
    {
        if (playerKeyword == null) return;
        if (keyType == WordBankBranch.NONE && playerKeyword != null)
        {
            keyType = WordBankBranch.ANY;
        }


        int totalKeyAnswer = b.GetTotalKeyAnswer(); // Total Keyword that can be use to answer this node.
        switch (keyType)
        {
            case WordBankBranch.INSPECT:
                for (int i = 0; i < totalKeyAnswer; i++)
                {
                    if (keyType == b.GetKeyWordSet(i).WordSet)
                    {
                        if (playerKeyword == b.GetKeyWordSet(i).keyAnswer)
                        {
                            _comDialogueReader.CustomNextNode(b.GetBranchOutputPortName(), i);
                        }
                    }
                }
                break;
            case WordBankBranch.INTERACT:
                for (int i = 0; i < totalKeyAnswer; i++)
                {
                    if (keyType == b.GetKeyWordSet(i).WordSet)
                    {
                        if (playerKeyword == b.GetKeyWordSet(i).keyAnswer)
                        {
                            _comDialogueReader.CustomNextNode(b.GetBranchOutputPortName(), i);
                        }
                    }
                }
                break;
            case WordBankBranch.MOVE:
                for (int i = 0; i < totalKeyAnswer; i++)
                {
                    if (keyType == b.GetKeyWordSet(i).WordSet)
                    {
                        if (playerKeyword == b.GetKeyWordSet(i).keyAnswer)
                        {
                            _comDialogueReader.CustomNextNode(b.GetBranchOutputPortName(), i);
                        }
                    }
                }
                break;
            case WordBankBranch.ANY:
                for (int i = 0; i < totalKeyAnswer; i++)
                {
                    if (keyType == b.GetKeyWordSet(i).WordSet)
                    {
                        if (playerKeyword != null)
                        {
                            _comDialogueReader.CustomNextNode(b.GetBranchOutputPortName(), i);
                        }
                    }
                }
                break;
        }
    }
    public string PlayerTypedText // Get typed text
    {
        get
        {
            return playerWord;
        }
    }

    public void PlayerTypingSound() // Play during the typing of the word
    {
        if (_inputTextbox.text.Length >= 1)
        {
            _soundDelayCounter--;

            if (_soundDelayCounter <= 0)
            {
                int randomSound = Random.Range(0, _typingSound.Length);
                _typingAudioSource.pitch = (Random.Range(1, 1.5f));
                _typingAudioSource.PlayOneShot(_typingSound[randomSound]);
                _soundDelayCounter = 1;
            }
        }
    }
}
[System.Serializable]public class WordBank
{
    [Header("Word Bank")]
    [SerializeField] private string[] _inspect;
    [SerializeField] private string[] _interact;
    [SerializeField] private string[] _move;
    public string[] inspect => _inspect;
    public string[] interact => _interact;
    public string[] move => _move;
}
public enum WordBankBranch
{
    NONE,
    INSPECT,
    INTERACT,
    MOVE,
    ANY
};