using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class MZ_Controller : MonoBehaviour
{
    [Header("Computer Settings")]
    [SerializeField] private bool _useStartPos = true;
    [SerializeField] private Vector3 _camStartPos;
    private bool _isBeingUse = false;

    [Header("References")]
    [SerializeField] private string _playerCamTag = "PlayerCamera";
    [SerializeField] private string _mainCamTag = "MainCamera";
    private MZ_WordListener mzListener;
    private ComputerDialogueReader _ComputerDialogueReader;
    private CinemachineVirtualCamera _computerCam;
    private CinemachineVirtualCamera _playerCam;
    private CinemachineBrain _mainCam;
    private PlayerBrain _playerbrain => PlayerBrain.Instance;
    private PlayerController _playerController => _playerbrain.playerController;
    public bool isBeingUse => _isBeingUse;

    private void Start()
    {
        mzListener = GetComponent<MZ_WordListener>();
        _ComputerDialogueReader = GetComponent<ComputerDialogueReader>();
        _computerCam = GetComponentInChildren<CinemachineVirtualCamera>();
        _playerCam = GameObject.FindGameObjectWithTag(_playerCamTag).GetComponent<CinemachineVirtualCamera>();
        _mainCam = GameObject.FindGameObjectWithTag(_mainCamTag).GetComponent<CinemachineBrain>();

        if (_useStartPos) _computerCam.transform.localPosition = _camStartPos;
    }
    private void Update()
    {
        if (_isBeingUse && !PlayerInterfaceManager.Instance.isPaused)
        {
            Cursor.lockState = CursorLockMode.Confined;
        }
    }
    private void OnBeingUse()
    {
        if (InputController.Instance.Tab)
        {
            ChangeCameraPriority(false);
        }
    }
    public void ExitPC() // Stop using the gameobject
    {
        ChangeCameraPriority(false);
    }
    public void ChangeCameraPriority(bool isBeingUse)
    {
        _isBeingUse = isBeingUse;

        // This is for not blending animation
        /*switch (_isBeingUse)
        {
            case true:
                mzListener.inputfield.interactable = true;
                _computerCam.Priority = 100;
                _playerCam.Priority = 10;
                Cursor.lockState = CursorLockMode.Confined;
                _playerController.MovementState = playerMovementState.UNMOVEABLE;
                break;
            case false:
                mzListener.inputfield.interactable = false;
                _computerCam.Priority = 10;
                _playerCam.Priority = 100;
                Cursor.lockState = CursorLockMode.Locked;
                _playerController.MovementState = playerMovementState.MOVEABLE;
                break;
        }*/
        StartCoroutine(ChangeCamWithBlendCheck(_isBeingUse));
    }
    IEnumerator ChangeCamWithBlendCheck(bool isBeingUse)    // Will check and do function after blending is completed
    {
        switch (isBeingUse)
        {
            case true:
                _computerCam.Priority = 100;
                _playerCam.Priority = 10;
                Cursor.lockState = CursorLockMode.Confined;
                _playerController.MovementState = playerMovementState.UNMOVEABLE;
                mzListener.inputfield.interactable = true;
                break;
            case false:
                _computerCam.Priority = 10;
                _playerCam.Priority = 100;
                break;
        }
        yield return null;
        yield return null;
        while (_mainCam.IsBlending) { yield return null; }

        switch (isBeingUse)
        {
            case true:
                //mzListener.inputfield.interactable = true;
                break;
            case false:
                Cursor.lockState = CursorLockMode.Locked;
                _playerController.MovementState = playerMovementState.MOVEABLE;
                mzListener.inputfield.interactable = false;
                PlayerInventoryUI.Instance.CloseInventory();
                break;
        }
    }  

    //Function
    public void UseTape(ComputerDialogueGraph newGraph)
    {
        _ComputerDialogueReader.StartNewDialogue(newGraph);
    }
}
