using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using DG.Tweening;

public class TriggerZoneClass_AudioZone : TriggerZoneClass
{
    [SerializeField] private AudioFade[] _fadeOnEnter;
    [SerializeField] private AudioFade[] _fadeOnExit;
    [System.Serializable]
    private class AudioFade
    {
        [SerializeField] private List<AudioSource> _targetAudio;
        [SerializeField] float _targetFade;
        [SerializeField] float _fadeDuration;

        public List<AudioSource> targetAudio => _targetAudio;
        public float targetFade => _targetFade;
        public float fadeDuration => _fadeDuration;
    }

    public override void OnTriggerEnter(Collider other)
    {
        bool foundTag = false;
        for (int i = 0; i < _triggerTags.Length; i++)
        {
            if (other.tag == _triggerTags[i]) foundTag = true; break;
        }
        if (!foundTag) return;
        _triggered = true;

        for (int i = 0; i < _fadeOnEnter.Length; i++)
        {
            for (int l = 0; l < _fadeOnEnter[i].targetAudio.Count; l++)
            {
                if (_fadeOnEnter[i].targetAudio[l].volume != _fadeOnEnter[i].targetFade)
                {
                    _fadeOnEnter[i].targetAudio[l].DOFade(_fadeOnEnter[i].targetFade, _fadeOnEnter[i].fadeDuration);
                }
            }
        }
    }
    public override void OnTriggerExit(Collider other)
    {
        //Debug.Log("Exit");
        bool foundTag = false;
        for (int i = 0; i < _triggerTags.Length; i++)
        {
            if (other.tag == _triggerTags[i]) foundTag = true; break;
        }
        if (!foundTag) return;
        _triggered = true;

        for (int i = 0; i < _fadeOnExit.Length; i++)
        {
            for (int l = 0; l < _fadeOnExit[i].targetAudio.Count; l++)
            {
                if (_fadeOnEnter[i].targetAudio[l].volume != _fadeOnExit[i].targetFade)
                {
                    //Debug.Log("Fade");
                    _fadeOnExit[i].targetAudio[l].DOFade(_fadeOnExit[i].targetFade, _fadeOnExit[i].fadeDuration);
                }
            }
        }
    }
}
