public interface IGameEventListener
{
    void OnEventRaised(string eventName);
}
