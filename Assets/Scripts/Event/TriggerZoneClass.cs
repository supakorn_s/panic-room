using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TriggerZoneClass : MonoBehaviour
{
    // Use this to make inheritance

    [Header("Trigger Settings")]
    [SerializeField] protected bool _triggerOnce = true; // Trigger only once
    [SerializeField] protected bool _destroyOnTrigger = true; //Destroy this trigger when trigger
    [SerializeField][TagSelector] protected string[] _triggerTags = new[] { "Player" };  // Tag that can be trigger
    protected bool _triggered = false;

    public virtual void OnTriggerEnter(Collider other)
    {
        return;
    }
    public virtual void OnTriggerStay(Collider other)
    {
        return;
    }
    public virtual void OnTriggerExit(Collider other)
    {
        return;
    }
    public virtual void DestroyCheck()
    {
        if (_destroyOnTrigger && _triggered)
        {
            Destroy(this.gameObject);
        }
    }
    public virtual bool FindTag(Collider other)
    {
        bool foundTag = false;
        for (int i = 0; i < _triggerTags.Length; i++)
        {
            if (other.tag == _triggerTags[i]) foundTag = true; break;
        }
        if (!foundTag) return foundTag;

        foundTag = true;
        return foundTag;
    }
}
