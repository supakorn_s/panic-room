using UnityEngine;
using UnityEngine.Events;

public class UnitygameEventListener : MonoBehaviour, IGameEventListener
{
    //[SerializeField] private GameEventClass _events; // Event to listen to
    //[SerializeField] private UnityEvent _response;  // Response to invoke when event is raised

    [SerializeField] private EventSet[] _event;
    public EventSet[] eventSet => _event;

    public void OnEnable()
    {

        //if (_events != null) _events.RegisterListener(this);  // For single event
        for (int i = 0; i < _event.Length; i++)
        {
            if (_event[i].events != null)
            {
                _event[i].events.RegisterListener(this);
            }
        }
    }
    public void OnDisable()
    {
        //_events.UnRegisterListener(this); // For single event
        for (int i = 0; i < _event.Length; i++)
        {
            if (_event[i].events != null) _event[i].events.UnRegisterListener(this);
        }
    }
    private void OnValidate()
    {
        for (int i = 0; i < _event.Length; i++)
        {
            if (_event[i].events != null)
            {
                _event[i].events.RegisterListener(this);
                _event[i].elementName = _event[i].events.name;
            }
        }
    }
    public void OnEventRaised(string eventName)
    {
        //_response?.Invoke();  // For single event, don't need string eventName
        for (int i = 0; i < _event.Length; i++)
        {
            if (_event[i].events.name == eventName) _event[i].response?.Invoke();
        }
    }
}

[System.Serializable]
public class EventSet
{
    [HideInInspector] public string elementName;
    [SerializeField] private GameEventClass _events; // Event to listen to
    [SerializeField] private UnityEvent _response;  // Response to invoke when event is raised
    public GameEventClass events => _events;
    public UnityEvent response => _response;

}
