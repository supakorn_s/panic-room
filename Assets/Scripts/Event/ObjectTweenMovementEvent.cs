using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObjectTweenMovementEvent : MonoBehaviour
{
    [SerializeField] private MovementSet[] _moveSetEvent;
    [SerializeField] private MovementSet[] _rotateSetEvent;

    public void TweenMovement(int moveSetEventNumber)   // Move object toward destination
    {
        if (_moveSetEvent[moveSetEventNumber] == null)
        {
            Debug.LogError("_rotateSetEvent " + moveSetEventNumber + " is missing");
            return;
        }
        if (_moveSetEvent[moveSetEventNumber].useLocalPos)
        {
            gameObject.transform.DOLocalMove(_moveSetEvent[moveSetEventNumber].tweenLocation, _moveSetEvent[moveSetEventNumber].tweenTime);
        }
        else
        {
            gameObject.transform.DOMove(_moveSetEvent[moveSetEventNumber].tweenLocation, _moveSetEvent[moveSetEventNumber].tweenTime);
        }
    }
    public void TweenRotation(int rotateEventNumber) // Rotate object toward destination
    {
        if (_rotateSetEvent[rotateEventNumber] == null)
        {
            Debug.LogError("_rotateSetEvent " + rotateEventNumber + " is missing");
            return;
        }
        gameObject.transform.DORotate(_rotateSetEvent[rotateEventNumber].tweenLocation, _rotateSetEvent[rotateEventNumber].tweenTime, RotateMode.Fast);
    }

    [System.Serializable]
    private class MovementSet
    {
        [SerializeField] private Vector3 _tweenLocation;
        [SerializeField] private float _tweenTime = 1.0f;
        [SerializeField] private bool _useLocalPos = false;

        public Vector3 tweenLocation => _tweenLocation;
        public float tweenTime => _tweenTime;
        public bool useLocalPos => _useLocalPos;
    }
}
