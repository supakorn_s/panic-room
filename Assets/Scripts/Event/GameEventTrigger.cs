using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventTrigger : MonoBehaviour
{
    [Header("Events to play")]
    [SerializeField] private GameEventClass[] _event;

    [Header("Tags to Trigger")]
    [SerializeField] private bool _triggerOnce = false;
    [SerializeField] private bool _destroyOnTrigger = false;
    [SerializeField][TagSelector] private string[] _triggerTags = new[] { "Player" };
    private bool _triggered = false;

    private void OnTriggerEnter(Collider other)
    {
        if (_triggerTags == null) return;
        if (_triggerOnce == true && _triggered == true) return;

        foreach (string triggerTags in _triggerTags)
        {
            if (other.CompareTag(triggerTags))
            {
                for (int i = 0; i < _event.Length; i++)
                {
                    _event[i].Raise();
                }
                _triggered = true;
            }
        }

        if (_triggered && _destroyOnTrigger) Destroy(this.gameObject);
    }
}
