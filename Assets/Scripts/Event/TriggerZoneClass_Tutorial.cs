using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerZoneClass_Tutorial : TriggerZoneClass
{
    [Header("Tutorial Numbers")]
    [SerializeField] private int _tutorialToPlay;
    private TutorialInterfaceEvent _tutorialManager;
    private void Start()
    {
        _tutorialManager = TutorialInterfaceEvent.Instance;
    }
    public override void OnTriggerEnter(Collider other)
    {
        if (_tutorialManager == null) return;
        if (FindTag(other) == false) return;

        _triggered = true;

        _tutorialManager.PlayTutorial(_tutorialToPlay);

        DestroyCheck();

    }
}
