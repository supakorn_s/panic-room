using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingManager : MonoBehaviour
{
    [SerializeField] private Animator _endingAnim;
    [SerializeField] private AnimationClip _endingClip;

    private void Awake()
    {
        
    }
    public void PlayEnding()
    {
        _endingAnim.Play(_endingClip.name.ToString());
    }
    public void RestartLevel()
    {
        SceneLoader.Instance.startRestartLevel();
    }
}
