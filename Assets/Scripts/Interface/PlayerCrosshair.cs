using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCrosshair : MonoBehaviour
{
    [Header("Crosshair")]
    [SerializeField] private Image _crosshair;
    [SerializeField] private Sprite _normalCrosshair;
    [SerializeField] private Sprite _pickupCrosshair;

    [Header("References")]
    [SerializeField] private PlayerBrain _playerBrain;
    private PlayerRaycast _playerRaycast;

    private void Start()
    {
        _playerRaycast = _playerBrain.playerRaycast;
    }
    private void Update()
    {
        CrosshairCheck();
    }

    private void CrosshairCheck()
    {
        switch (_playerBrain.playerController.MovementState)
        {
            case playerMovementState.MOVEABLE:
                if (_playerRaycast.hitObject == null && _normalCrosshair != null)
                {
                    _crosshair.sprite = _normalCrosshair;
                }
                else
                {
                    _crosshair.enabled = false;
                    _crosshair.sprite = null;
                }
                if (_playerRaycast.hitObject != null && _pickupCrosshair != null)
                {
                    _crosshair.enabled = true;
                    _crosshair.sprite = _pickupCrosshair;
                }
                break;
            case playerMovementState.UNMOVEABLE:
                _crosshair.enabled = false;
                _crosshair.sprite = null;
                break;
        }
    }
}
