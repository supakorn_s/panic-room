using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemIcon : MonoBehaviour, IDragHandler, IEndDragHandler, IDropHandler, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [Header("Item Information")]
    public ItemProfileClass _itemProfile;
    [Header("Image Alpha")]
    [SerializeField] private Image _itemIcon;
    [SerializeField] private CanvasGroup _itemCanvasGroup;
    [SerializeField] private float _normalAlpha = 1.0f;
    [SerializeField] private float _onHoverAlpha = 0.75f;
    [SerializeField] private float _onDragAlpha = 0.4f;
    public GameObject slotParent; // For debug only

    [Header("Audio")]
    [SerializeField] private AudioSource _soundEffectSource;
    [SerializeField] private AudioClip _effectOnClick;

    private Canvas _currentCanvas;
    private GameObject _currentCanvasGameObject;
    private RectTransform _rect;

    private void Start()
    {
        slotParent = transform.parent.gameObject;
        _rect = GetComponent<RectTransform>();

        _currentCanvas = GetComponentInParent(typeof(Canvas)) as Canvas;
        _currentCanvasGameObject = _currentCanvas.gameObject;

        if (_itemProfile != null)
        {
            _itemIcon.sprite = _itemProfile.itemIcon;
        }
    }
    private void OnEnable()
    {
        _itemCanvasGroup.alpha = _normalAlpha;
    }
    private void ResetPosition()
    {
        gameObject.transform.SetParent(slotParent.transform);
        transform.localPosition = new Vector2(0, 0);
    }
    private void SwapPosition(ItemIcon OppositeItemIcon)
    {
        GameObject currentSlot = slotParent;
        GameObject oppositeItem = OppositeItemIcon.gameObject;
        ItemSlot currentSlotScript = currentSlot.GetComponent<ItemSlot>();
        ItemSlot oppositeItemSlotScript = oppositeItem.GetComponent<ItemIcon>().slotParent.GetComponent<ItemSlot>();
        ItemProfileClass currentItem = currentSlotScript._itemProfile;
        ItemProfileClass replaceItem = oppositeItemSlotScript._itemProfile;

        gameObject.transform.SetParent(OppositeItemIcon.slotParent.transform);
        oppositeItem.transform.SetParent(slotParent.transform);
        gameObject.transform.localPosition = new Vector2(0, 0);
        oppositeItem.transform.localPosition = new Vector2(0, 0);

        currentSlotScript._itemProfile = replaceItem;
        oppositeItemSlotScript._itemProfile = currentItem;

        slotParent = OppositeItemIcon.slotParent;
        OppositeItemIcon.slotParent = currentSlot;
        PlayerInventoryUI.Instance.playerInventory.InventorySlotUpdate();
    }
    public void ChangePosition(ItemSlot OverlapSlot)
    {
        gameObject.transform.SetParent(OverlapSlot.gameObject.transform);
        gameObject.transform.localPosition = new Vector2(0, 0);
        slotParent = OverlapSlot.gameObject;
    }

    #region Pointer Event
    public void OnDrag(PointerEventData eventdata)
    {
        gameObject.transform.SetParent(_currentCanvasGameObject.transform);
        _rect.anchoredPosition += eventdata.delta / _currentCanvas.scaleFactor;

        _itemCanvasGroup.alpha = _onDragAlpha;
        _itemCanvasGroup.blocksRaycasts = false;

        PlayerInventoryUI.Instance.SetItemInspectionTabActive(false);
    }
    public void OnEndDrag(PointerEventData eventdata)
    {
        _itemCanvasGroup.alpha = _normalAlpha;
        _itemCanvasGroup.blocksRaycasts = true;

        if (eventdata.pointerDrag != null)
        {
            GameObject eventdataObject;
            eventdataObject = eventdata.pointerDrag.gameObject;

            if (eventdataObject.TryGetComponent(out ItemSlot itemSlot) == false)
            {
                ResetPosition();
            }

        }
    }
    public void OnDrop(PointerEventData eventdata)
    {
        GameObject eventdataObject;
        eventdataObject = eventdata.pointerDrag.gameObject;
        if (eventdataObject.TryGetComponent(out ItemIcon OverlapItemIcon) == true)
        {
            SwapPosition(OverlapItemIcon);
        }
        else if (eventdataObject.TryGetComponent(out ItemSlot OverlapSlot) == true)
        {
            ChangePosition(OverlapSlot);
        }
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        _itemCanvasGroup.alpha = _onHoverAlpha;
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        _itemCanvasGroup.alpha = _normalAlpha;
    }
    public void OnPointerDown(PointerEventData eventdata)
    {
        PlayerInventoryUI.Instance.UpdateItemDetail(_itemProfile);

        if (slotParent.TryGetComponent(out ItemSlot currentSlot))
        {
            PlayerInventoryUI.Instance.SpawnItemInspectionButton(_itemProfile, currentSlot);
        }

        UIOnClickEffect();
    }
    #endregion


    #region Audio
    public void UIOnClickEffect()
    {
        _soundEffectSource.PlayOneShot(_effectOnClick);
    }
    #endregion
}
