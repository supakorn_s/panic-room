using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ItemPickUpNotification : SingletonClass<ItemPickUpNotification>
{
    [SerializeField] private GameObject _pickupDisplay;
    [SerializeField] private GameObject _notificationParent;
    [SerializeField] private GameObject _notificationPrefab;
    [SerializeField] private float _displayTime = 4.0f;
    private TextMeshProUGUI[] _parentChildren;

    private void Update()
    {
        _parentChildren = _notificationParent.GetComponentsInChildren<TextMeshProUGUI>();
        if (_parentChildren.Length <= 0)
        {
            _pickupDisplay.SetActive(false);
        }
    }

    public void SpawnItemNotification(ItemProfileClass itemProfile)
    {
        GameObject notification = Instantiate(_notificationPrefab, _notificationParent.transform, _notificationParent);
        TextMeshProUGUI notiText = null;
        notiText = notification.GetComponentInChildren<TextMeshProUGUI>();
        if (notiText != null)
        {
            notiText.text = itemProfile.itemName;
        }
        Destroy(notification, _displayTime);
        _pickupDisplay.SetActive(true);
    }
}
