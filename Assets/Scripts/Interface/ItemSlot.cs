using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemSlot : MonoBehaviour, IDropHandler
{
    [Header("Item Information")]
    public ItemProfileClass _itemProfile;
    [SerializeField] private GameObject itemIconPrefab;

    [Header("Slot Information")]
    public int slotNumber = -10;

    public void OnDrop(PointerEventData eventdata)
    {
        GameObject eventdataObject;
        eventdataObject = eventdata.pointerDrag.gameObject;
        if (eventdataObject.TryGetComponent(out ItemIcon OverlapItemIcon) == true)
        {
            OverlapItemIcon.slotParent.GetComponent<ItemSlot>()._itemProfile = null;
            OverlapItemIcon.ChangePosition(gameObject.GetComponent<ItemSlot>());
            _itemProfile = OverlapItemIcon._itemProfile;

            PlayerInventoryUI.Instance.playerInventory.InventorySlotUpdate();
        }
    }
    public void AddItemProfile(ItemProfileClass newItemProfile)
    {
        _itemProfile = newItemProfile;
        GameObject itemIcon = Instantiate(itemIconPrefab);
        itemIcon.transform.SetParent(this.transform, false);

        itemIcon.transform.position = this.transform.position;
        if (itemIcon.TryGetComponent(out ItemIcon itemIconScript)) itemIconScript._itemProfile = newItemProfile;
    }
}
