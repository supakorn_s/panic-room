using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerInterfaceManager : SingletonClass<PlayerInterfaceManager>, IGameEventListener
{
    public bool isPaused = false;
    [Header("Canvas")]
    [SerializeField] private GameObject _pauseCanvas;
    [SerializeField] private GameObject _menuTab;
    [SerializeField] private GameObject _settingTab;
    [SerializeField] private GameObject[] _allCanvas;
    //[SerializeField] private ResolutionSetting _ResolutionSetting;
    [Header("Call Events to activate/deactivate canvas")]
    [SerializeField] private GameEventClass _onPause;
    [SerializeField] private GameEventClass _onResume;
    [SerializeField] private GameEventClass _onSettings;
    [SerializeField] private EventSet[] _event;

    [Header("Settings manager")]
    [SerializeField] private ResolutionSetting _resolutionSetting;

    [Header("Audio")]
    [SerializeField] private AudioSource _soundEffectSource;
    [SerializeField] private AudioClip _effectOnClick;

    void Start()
    {
        _resolutionSetting.dropDownCurrentStartResolution();
        isPaused = false;
        for (int i = 0; i < _allCanvas.Length; i++)
        {
            _allCanvas[i].SetActive(false);
        }
        _onResume.Raise();
    }
    public void OnEnable()
    {
        for (int i = 0; i < _event.Length; i++)
        {
            if (_event[i].events != null)
            {
                _event[i].events.RegisterListener(this);
            }
        }
    }
    public void OnDisable()
    {
        for (int i = 0; i < _event.Length; i++)
        {
            if (_event[i].events != null) _event[i].events.UnRegisterListener(this);
        }
    }
    private void OnValidate()
    {
        for (int i = 0; i < _event.Length; i++)
        {
            if (_event[i].events != null)
            {
                _event[i].events.RegisterListener(this);
                _event[i].elementName = _event[i].events.name;
            }
        }
    }
    void Update()
    {
        OpenCloseMenuTab();
    }
    private void OpenCloseMenuTab()
    {
        if (InputController.Instance.ESC)
        {
            if (isPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }
    private void Pause()
    {
        _onPause.Raise();
        isPaused = true;
        Time.timeScale = 0.0f;
        Cursor.lockState = CursorLockMode.Confined;
    }
    public void Resume()
    {
        _onResume.Raise();
        isPaused = false;
        Time.timeScale = 1f;
        Cursor.lockState = CursorLockMode.Locked;
    }
    public void ForceExit()
    {
        Application.Quit();
    }
    public void OnEndingLevel()
    {
        isPaused = true;
        Time.timeScale = 0.0f;
        Cursor.lockState = CursorLockMode.Confined;
    }
    public void OnEventRaised(string eventName)
    {
        for (int i = 0; i < _event.Length; i++)
        {
            if (_event[i].events.name == eventName) _event[i].response?.Invoke();
        }
    }
    public void UIOnClickEffect()
    {
        _soundEffectSource.PlayOneShot(_effectOnClick);
    }
}
