using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerInventoryUI : SingletonClass<PlayerInventoryUI>
{
    [Header("Inventory Canvas")]
    [SerializeField] private bool _inventoryIsOpen = false;
    [SerializeField] private GameObject _playerInvantory;
    public bool inventoryIsOpen => _inventoryIsOpen;

    [Header("Inventory Details")]
    [SerializeField] private TextMeshProUGUI _itemName;
    [SerializeField] private TextMeshProUGUI _itemDescription;

    [Header("Inventory Items")]
    [SerializeField] private GameObject _inventorySlotPrefab;
    [SerializeField] private GameObject _itemSlotContentFolder;
    [SerializeField] private List<ItemSlot> _itemSlot;
    public List<ItemSlot> itemSlot => _itemSlot;

    [Header("Item Inspection Group Tab")]
    [SerializeField] private GameObject _itemInspectionTab;
    [SerializeField] private Vector3 __itemInspectionTabOffset;

    [SerializeField] private GameObject _useSelection;
    [SerializeField] private GameObject _dropSelection;

    [SerializeField] private List<GameObject> _inspectionButtons;
    public List<GameObject> inspectionButtons => _inspectionButtons;

    [Header("Use Item Function")]
    [SerializeField] private string _cannotUseItemText = "Item cannot be use here";
    [SerializeField] private float _cannotUseItemTime = 2.0f;

    [Header("Use Item String Function")]
    [TagSelector][SerializeField] private string _mainComputerTag;
    [SerializeField] private string _useChangeGraph = "ChangeDialogueProfile";

    [Header("Audio")]
    [SerializeField] private AudioSource _soundEffectSource;
    [SerializeField] private AudioClip _effectOnClick;

    //[Header("Player References")]
    private PlayerBrain _playerBrain;
    private PlayerController _playerController;
    private PlayerInventory _playerInventory;
    public PlayerInventory playerInventory => _playerInventory;

    private void Start()
    {
        _playerBrain = FindObjectOfType<PlayerBrain>();
        _playerController = _playerBrain.playerController;
        _playerInventory = _playerBrain.playerInventory;

        StartUpSpawnInventorySlot();
        //CloseInventory();
    }
    private void Update()
    {
        OpenCloseInventory();
    }
    
    #region Open/Close Inventory
    private void OpenCloseInventory()
    {
        if (InputController.Instance.Tab && _playerController.isGrounded && !PlayerPasswordUI.Instance.PWPanelOpen)
        {
            if (inventoryIsOpen)
            {
                CloseInventory();
            }
            else
            {
                OpenInventory();
            }
        }
        if (InputController.Instance.ESC)
        {
            CloseInventory();
        }
    }
    public void OpenInventory()
    {
        _inventoryIsOpen = true;

        _playerInvantory.SetActive(true);
        ClearItemDetail();
        _playerController.MovementState = playerMovementState.UNMOVEABLE;
        Cursor.lockState = CursorLockMode.Confined;
    }
    public void CloseInventory()
    {
        _inventoryIsOpen = false;

        _playerInvantory.SetActive(false);
        _itemInspectionTab.SetActive(false);
        _playerController.MovementState = playerMovementState.MOVEABLE;
        Cursor.lockState = CursorLockMode.Locked;
    }
    #endregion

    #region Inventory Slot
    public int CheckTheFirstFreeSlot    // Check inventory first free slot
    {
        get
        {
            int freeslot = -100;

            for (int i = 0; i < _itemSlot.Count; i++)
            {
                if (_itemSlot[i].transform.childCount == 1)
                {
                    freeslot = i;
                    break;
                }
            }
            return freeslot;
        }
    }
    private void StartUpSpawnInventorySlot()    //Spawn inventory slot to match the player data
    {
        foreach (Transform child in _itemSlotContentFolder.transform) // Clear temp inventory slot or current slot
        {
            Destroy(child.gameObject);
        }
        _itemSlot.Clear();

        if (_playerInventory.totalInventorySlot < 0)
        {
            Debug.LogWarning("Total inventory slot is below 0");
            return;
        }
        for (int i = 0; i < _playerInventory.totalInventorySlot; i++)
        {
            GameObject newSlot = Instantiate(_inventorySlotPrefab, _itemSlotContentFolder.transform, _itemSlotContentFolder);
            newSlot.name = "Item Slot (" + i + ")";

            if (newSlot.TryGetComponent(out ItemSlot spawnedSlot))
            {
                _itemSlot.Add(spawnedSlot);
                spawnedSlot.slotNumber = i;
                _playerInventory.currentItem.Add(spawnedSlot._itemProfile);
            }
        }

        _inventoryIsOpen = false;

        _playerInvantory.SetActive(false);
        _itemInspectionTab.SetActive(false);
    }
    #endregion

    public void UpdateItemDetail(ItemProfileClass _itemProfile)
    {
        _itemName.text = _itemProfile.itemName;
        _itemDescription.text = _itemProfile.itemDescription;
    }
    private void ClearItemDetail()
    {
        _itemName.text = null;
        _itemDescription.text = null;
    }
    public void DeleteItem(ItemProfileClass _itemProfile)
    {
        for (int i = 0; i < _itemSlot.Count; i++)
        {
            if (_itemSlot[i]._itemProfile == _itemProfile)
            {
                _itemSlot[i]._itemProfile = null;
                _playerInventory.InventorySlotUpdate();
                Destroy(_itemSlot[i].gameObject.transform.GetChild(1).gameObject);
                break;
            }
        }
    }
    public void UseItem(ItemProfileClass _itemProfile, ItemSlot currentSlot)
    {
        _itemInspectionTab.SetActive(false);

        UseTapeToChangeDialogue(_itemProfile);
    }
    private void DropItem(ItemProfileClass _itemProfile, ItemSlot currentSlot)
    {
        currentSlot._itemProfile = null;
        _playerInventory.InventorySlotUpdate();
        if (_itemProfile.itemPrefab == null)
        {
            Debug.LogWarning("No itemPrefab in itemProfile");
            Destroy(currentSlot.gameObject.transform.GetChild(1).gameObject);
            _itemInspectionTab.SetActive(false);
            return;
        }

        _playerInventory.SpawnDropItem(_itemProfile);
        Destroy(currentSlot.gameObject.transform.GetChild(1).gameObject);

        _itemInspectionTab.SetActive(false);
        _itemName.text = null;
        _itemDescription.text = null;
    }
    public void SetItemInspectionTabActive(bool active)
    {
        _itemInspectionTab.SetActive(active);
    }
    public void SpawnItemInspectionButton(ItemProfileClass _itemProfile, ItemSlot itemSlot)
    {
        _itemInspectionTab.SetActive(true);

        for (int i = 0; i < _inspectionButtons.Count; i++)
        {
            _inspectionButtons[i].SetActive(true);
            if (_inspectionButtons[i].TryGetComponent(out Button _button))
            {
                _button.onClick.RemoveAllListeners();

                switch (i)
                {
                    case 0:
                        if (_itemProfile.usable)
                        {
                            _useSelection.SetActive(true);
                            _button.onClick.AddListener(delegate { UseItem(_itemProfile, itemSlot); });
                        }
                        else
                        {
                            _useSelection.SetActive(false);
                        }
                        break;

                    case 1:
                        if (_itemProfile.droppable)
                        {
                            _dropSelection.SetActive(true);
                            _button.onClick.AddListener(delegate { DropItem(_itemProfile, itemSlot); });
                        }
                        else
                        {
                            _dropSelection.SetActive(false);
                        }
                        break;
                }
            }
        }
        //Transform button position
        _itemInspectionTab.transform.position = itemSlot.gameObject.transform.position + __itemInspectionTabOffset;
    }

    #region Audio
    public void UIOnClickEffect()
    {
        _soundEffectSource.PlayOneShot(_effectOnClick);
    }
    #endregion


    #region Use item Function   // Function when pressing using item
    private GameObject CheckRayHitObject
    {
        get
        {
            GameObject hitObject = _playerBrain.playerRaycast.hitObject;
            return hitObject;
        }
    }
    private void CannotUseItemSubtitle()    // Show text that it cannot be use here.
    {
        //Debug.Log("Insert subtitle for not able to use item here");
        SubtitleDisplay.Instance.TypeSubtitle(_cannotUseItemText, _cannotUseItemTime);
        CloseInventory();
    }
    private void UseTapeToChangeDialogue(ItemProfileClass _itemProfile)
    {
        GameObject hitObject = CheckRayHitObject;
        if (hitObject == null)
        {
            CannotUseItemSubtitle();
            return;
        }
        
        if (hitObject.TryGetComponent(out MZ_Controller computer))
        {
            if (_itemProfile.UseItem() == _useChangeGraph) // ChangeDialogueProfile string
            {
                MZ_Controller mainComputer = GameObject.FindGameObjectWithTag(_mainComputerTag).GetComponent<MZ_Controller>();
                if (mainComputer == null) return;

                mainComputer.UseTape(_itemProfile.GetDialogue());
                if (_itemProfile.GetBool() == true)
                {
                    _playerInventory.DeleteItem(_itemProfile);
                }
            }
        }
        else
        {
            CannotUseItemSubtitle();
            return;
        }
    }
    #endregion


}
