using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialInterfaceEvent : SingletonClass<TutorialInterfaceEvent>
{
    [SerializeField] private Animator _tutorialAnim;
    [SerializeField] private AnimationClip[] _tutorialClip;

    public void PlayTutorial(int tutorialNum)
    {
        _tutorialAnim.Play(_tutorialClip[tutorialNum].name.ToString());
    }
}
