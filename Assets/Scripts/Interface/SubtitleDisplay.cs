using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using RedBlueGames.Tools.TextTyper;

public class SubtitleDisplay : SingletonClass<SubtitleDisplay>
{
    [Header("Subtitle Display")]
    [SerializeField] private GameObject _subtitleDisplay;
    [SerializeField] private TextTyper _textTyper;
    [SerializeField] private TextMeshProUGUI _subtitleBox;
    private bool _textisShowing = false;
    private float _subtitleTimer;

    private void Update()
    {
        if (_textisShowing)
        {
            if (_subtitleTimer > 0)
            {
                _subtitleTimer -= Time.deltaTime;
            }
            else
            {
                _subtitleDisplay.SetActive(false);
                _textisShowing = false;
                _subtitleBox.text = null;
            }
        }
    }

    public void TypeSubtitle(string newSubtitleText, float showTimer)
    {
        _subtitleDisplay.SetActive(true);
        _subtitleTimer = showTimer;
        _textisShowing = true;
        if (_subtitleBox.text != newSubtitleText)
        {
            _textTyper.TypeText(newSubtitleText);
        }
    }
}
