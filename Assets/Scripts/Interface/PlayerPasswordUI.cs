using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerPasswordUI : SingletonClass<PlayerPasswordUI>
{
    [Header("Password panel")]
    [SerializeField] private GameObject _passwordNumberPanel;
    [SerializeField] private TMP_InputField _passwordNumberField;
    private InteractableObject_NumberPasswordSafe _currentPWNumberPanel;


    [Header("Audio")]
    [SerializeField] private AudioSource _panelAudioSource;
    [SerializeField] private AudioClip _buttonClickSound;

    private bool _PWPanelOpen = false;
    public bool PWPanelOpen => _PWPanelOpen;
    private PlayerBrain _playerBrain;
    private PlayerController _playerController;


    private void Start()
    {
        _playerBrain = FindObjectOfType<PlayerBrain>();
        _playerController = _playerBrain.playerController;

        _passwordNumberPanel.SetActive(false);
    }
    private void Update()
    {
        OpenCloseChecker();
    }
    private void OpenCloseChecker()
    {
        if (InputController.Instance.ESC)
        {
            if (_PWPanelOpen)
            {
                CloseAPanel();
            }
        }
    }
    private void OpenAPanel()
    {
        _PWPanelOpen = true;
        _playerController.MovementState = playerMovementState.UNMOVEABLE;
        Cursor.lockState = CursorLockMode.Confined;
    }
    private void CloseAPanel()
    {
        _PWPanelOpen = false;
        _playerController.MovementState = playerMovementState.MOVEABLE;
        Cursor.lockState = CursorLockMode.Locked;

        _passwordNumberPanel.SetActive(false);
    }
    private void PlayOneShotWithRandomPitch(AudioClip ClipToPlay)
    {
        _panelAudioSource.pitch = Random.Range(0.8f, 1.2f);
        _panelAudioSource.PlayOneShot(ClipToPlay);
    }
    public void DeleteLastCharacter(TMP_InputField targetInpitField)
    {
        PlayOneShotWithRandomPitch(_buttonClickSound);

        string newText = targetInpitField.text;
        if (newText.Length <= 0) return;
        newText = newText.Substring(0, newText.Length - 1);
        targetInpitField.text = newText;
    }




    #region Number Password Panel
    // For Open/Close Password with Number Panel and their button functions.
    public void OpenPasswordNumberpanel(InteractableObject_NumberPasswordSafe TargetObject)
    {
        _passwordNumberPanel.SetActive(true);
        _passwordNumberField.characterLimit = TargetObject.password.ToString().Length;
        OpenAPanel();

        if (TargetObject != null)
        {
            _currentPWNumberPanel = TargetObject;
        }
    }
    public void ButtonClosePasswordNumberpanel()
    {
        if (_passwordNumberPanel.activeSelf == true)
        {
            _passwordNumberPanel.SetActive(false);
            CloseAPanel();
        }
    }
    public void TypingNumberPanel(int numberToType)
    {
        PlayOneShotWithRandomPitch(_buttonClickSound);
        string newText = _passwordNumberField.text;

        if (newText.Length >= _passwordNumberField.characterLimit) return;

        newText = _passwordNumberField.text + numberToType.ToString();
        _passwordNumberField.text = newText;
    }
    public void CheckingPasswordForNumberPanel()
    {
        if (_passwordNumberField.text == "")
        {
            PlayOneShotWithRandomPitch(_buttonClickSound);
            return;
        }
        if (_currentPWNumberPanel != null)
        {
            int attemptInput = int.Parse(_passwordNumberField.text);
            _passwordNumberField.text = null;
            _currentPWNumberPanel.CheckingPassWordInput(attemptInput);
        }
    }
    #endregion

}
