using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class EventDebugManager : MonoBehaviour
{
    public bool _enableDebuggingFunction = false;

    [SerializeField] private GameObject _debugButtonPrefab;
    [SerializeField] private GameObject _contentsParent;
    [SerializeField] private GameEventClass _onResume;
    [SerializeField] private GameEventClass _onDebugEvent;
    private UnitygameEventListener eventListener;

    private void Start()
    {
        eventListener = FindObjectOfType<UnitygameEventListener>();
        SetupEventbutton();
    }
    private void Update()
    {
        if (_enableDebuggingFunction)
        {
            OpenCloseDebugTab();
            if (InputController.Instance.DebugRestart)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }
    private void OpenCloseDebugTab()
    {
        if (InputController.Instance.DebugMenu)
        {
            if (PlayerInterfaceManager.Instance.isPaused)
            {
                Resume();
            }
            else
            {
                DebugOpen();
            }
        }
    }
    private void DebugOpen()
    {
        _onDebugEvent.Raise();
        PlayerInterfaceManager.Instance.isPaused = true;
        Time.timeScale = 0.0f;
        Cursor.lockState = CursorLockMode.Confined;
    }
    public void Resume()
    {
        _onResume.Raise();
        PlayerInterfaceManager.Instance.isPaused = false;
        Time.timeScale = 1f;
        Cursor.lockState = CursorLockMode.Locked;
    }
    public void EnableDebugFunction(bool enable)
    {
        _enableDebuggingFunction = enable;
    }
    void SetupEventbutton()
    {
        for (int i = 0; i < eventListener.eventSet.Length; i++)
        {
            GameObject debugButton = Instantiate(_debugButtonPrefab, _contentsParent.transform);
            gameObject.transform.localPosition = new Vector3(0, 0, 0);

            if (debugButton.TryGetComponent(out Button button))
            {
                int eventToCalled = i;
                button.onClick.AddListener(delegate { CalledEvent(eventToCalled); });
                debugButton.GetComponentInChildren<TextMeshProUGUI>().text = eventListener.eventSet[eventToCalled].events.name;
            }
        }
    }

    public void CalledEvent(int eventNumber)
    {
        eventListener.eventSet[eventNumber].events.Raise();
    }
}
