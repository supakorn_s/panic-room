using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Cinemachine;

public class MainMenuManager : MonoBehaviour, IGameEventListener
{
    private PlayerBrain _playerbrain;
    private PlayerController _playerController => _playerbrain.playerController;
    [Header("Main Menu")]
    [SerializeField] private bool _startWithMenu = false;

    [Header("Events for button to called")]
    [SerializeField] private GameEventClass _onBeginMenu;
    [SerializeField] private GameEventClass _onExitMenu;
    [SerializeField] private GameEventClass _onStart;
    [SerializeField] private GameEventClass _onSettings;
    [SerializeField] private GameEventClass _onCredit;
    [SerializeField] private GameEventClass _onReturn;
    [SerializeField] private EventSet[] _event;

    [Header("Camera")]
    [SerializeField] private CinemachineVirtualCamera _menuCam;
    [SerializeField] private CinemachineVirtualCamera _pDownCam;
    [SerializeField] private string _playerCamTag = "PlayerCamera";
    [SerializeField] private string _mainCamTag = "MainCamera";
    private CinemachineVirtualCamera _playerCam;
    private CinemachineBrain _mainCam;

    [Header("Settings manager")]
    [SerializeField] private ResolutionSetting _resolutionSetting;

    [Header("Starting Game Events")]
    [SerializeField] private float _facingDownTime = 2.0f;

    [Header("Audio")]
    [SerializeField] private AudioSource _soundEffectSource;
    [SerializeField] private AudioClip _effectOnClick;

    private void Start()
    {
        _playerbrain = FindObjectOfType<PlayerBrain>();
        _playerCam = GameObject.FindGameObjectWithTag(_playerCamTag).GetComponent<CinemachineVirtualCamera>();
        _mainCam = GameObject.FindGameObjectWithTag(_mainCamTag).GetComponent<CinemachineBrain>();
        _resolutionSetting.dropDownCurrentStartResolution();

        StartUpMenu();
    }
    #region OnEn/Dis able + Validate
    public void OnEnable()
    {
        for (int i = 0; i < _event.Length; i++)
        {
            if (_event[i].events != null)
            {
                _event[i].events.RegisterListener(this);
            }
        }
    }
    public void OnDisable()
    {
        for (int i = 0; i < _event.Length; i++)
        {
            if (_event[i].events != null) _event[i].events.UnRegisterListener(this);
        }
    }
    private void OnValidate()
    {
        for (int i = 0; i < _event.Length; i++)
        {
            if (_event[i].events != null)
            {
                _event[i].events.RegisterListener(this);
                _event[i].elementName = _event[i].events.name;
            }
        }
    }
    #endregion
    public void OnEventRaised(string eventName) // On calling event
    {
        for (int i = 0; i < _event.Length; i++)
        {
            if (_event[i].events.name == eventName) _event[i].response?.Invoke();
        }
    }
    private void StartUpMenu()
    {
        if (_startWithMenu)
        {
            _onBeginMenu.Raise();
            ChangeCameraPriority(true);
        }
        else
        {
            _onExitMenu.Raise();
        }
    }
    public void BeginGame()
    {
        StartCoroutine(BeginGameBlendInPlayerCamera());
    }
    public void ExitGame()
    {
        SceneLoader.Instance.exitToDestop();
    }
    public void ChangeCameraPriority(bool isBeingUse)
    {
        StartCoroutine(ChangeCamWithBlendCheck(isBeingUse));
    }
    IEnumerator ChangeCamWithBlendCheck(bool isBeingUse)    // Will check and do function after blending is completed
    {
        switch (isBeingUse)
        {
            case true:
                _menuCam.Priority = 100;
                _playerCam.Priority = 10;
                Cursor.lockState = CursorLockMode.Confined;
                _playerController.MovementState = playerMovementState.UNMOVEABLE;
                break;
            case false:
                _menuCam.Priority = 10;
                _playerCam.Priority = 100;
                break;
        }
        yield return null; 
        yield return null;
        while (_mainCam.IsBlending) { yield return null; }

        switch (isBeingUse)
        {
            case false:
                Cursor.lockState = CursorLockMode.Locked;
                _playerController.MovementState = playerMovementState.MOVEABLE;
                break;
        }
    }
    IEnumerator BeginGameBlendInPlayerCamera()
    {
        _menuCam.Priority = 10;
        _pDownCam.Priority = 100;

        yield return null;
        yield return null;
        while (_mainCam.IsBlending) { yield return null; }
        yield return new WaitForSeconds(_facingDownTime);

        _pDownCam.Priority = 10;
        _playerCam.Priority = 100;
        _onExitMenu.Raise();


        yield return null;
        yield return null;
        while (_mainCam.IsBlending) { yield return null; }

        Cursor.lockState = CursorLockMode.Locked;
        _playerController.MovementState = playerMovementState.MOVEABLE;

    }

    public void UIOnClickEffect()
    {
        _soundEffectSource.PlayOneShot(_effectOnClick);
    }
}
